INSERT INTO products (name, price) VALUES ('milk', 1.5);
INSERT INTO products (name, price) VALUES ('banana', 0.5);
INSERT INTO products (name, price) VALUES ('bread', 0.5);
INSERT INTO products (name, price) VALUES ('apple', 0.2);
INSERT INTO products (name, price) VALUES ('cherry', 0.7);
INSERT INTO products (name, price) VALUES ('strawberry', 0.6);
INSERT INTO products (name, price) VALUES ('tavernello', 0.99);
INSERT INTO products (name, price) VALUES ('wine', 1.75);
