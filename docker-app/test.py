import mysql.connector
import os
import socket
import sys
from function import *

assert connect() == True
assert price("milk") == 1.5
assert total_price("milk", 3) == 4.5
assert total_price('wine', 2) == 3.5
