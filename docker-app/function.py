import mysql.connector
import os
import time
import socket
import sys

def connect():


    print("########## TESTING DATABASE CONNECTION ##########")
    time.sleep(20)


    conn = mysql.connector.connect(
      host = "172.18.0.2",
      user = "root",
      passwd = "mypassword",
      database = "market"
    )

    print("CONNECTION OK")
    cur = conn.cursor()
    cur.execute("use market;")
    return True

def price(product):


    print("########## TESTING PRODUCT PRICE ##########")
    conn = mysql.connector.connect(
      host = "172.18.0.2",
      user = "root",
      passwd = "mypassword",
      database = "market"
    )

    print("CONNECTION OK")

    print("TESTING PRODUCT {}".format(product))
    cur = conn.cursor()

    cur.execute("use market;")

    sql = "SELECT price FROM products WHERE name = %s"
    name = (product,)
    cur.execute(sql, name)

    print("PRICE RETURNED")
    res = cur.fetchone()
    for element in res:
    	r = float(element)
    
    print("This product costs: {}$".format(r))
    print("PRICE RETURNED")
    return r

def total_price(product, quantity):

    print("########## TESTING TOTAL PRICE ##########")
    conn = mysql.connector.connect(
      host = "172.18.0.2",
      user = "root",
      passwd = "mypassword",
      database = "market"
    )
    print("CONNECTION OK")
    print("TESTING PRODUCT {}".format(product))
    print("QUANTITY {}".format(quantity))

    cur = conn.cursor()

    cur.execute("use market;")

    sql = "SELECT price FROM products WHERE name = %s"
    name = (product,)
    cur.execute(sql, name)

    res = cur.fetchone()

    for row in res:
    	print("The item you want to buy costs {} a unit".format(row))
    	pay = quantity * float(row)
    print("You want to buy {}".format(quantity) + " units of {}".format(product))
    print("You have to pay: {}$".format(pay))
    print("TOTAL RETURNED")
    conn.close()
    return float(pay)
