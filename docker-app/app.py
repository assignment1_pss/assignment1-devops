import mysql.connector
import os
import socket
import sys

#product = str(sys.argv[1])
#quantity = float(sys.argv[2])

product = str(raw_input('Enter the product you want to buy: '))
quantity = float(raw_input('Enter the quantity of that product: '))

conn = mysql.connector.connect(
	host="172.17.0.2",
	user="root",
	passwd="mypassword",
	database="market"
)

cur = conn.cursor()

sql = "SELECT price FROM products WHERE name = %s"
name = (product,)
cur.execute(sql, name)

res = cur.fetchone()

for row in res:
	print("The item you want to buy costs {} a unit".format(row))
	pay = quantity * float(row)
	
print("You have to pay: {}$".format(pay))

conn.close()
