# Assignment1 DevOps

The aim of this project is to make an available application running in DevOps scenario.
The first step is to set up a proper development environment.
Since our team is composed by 2 developer, the application can be distriuted or monolithic. Our choose is to build a distributed application.
The application must include at least two asptects from the following list:
1. Containerization/Virtualization (e.g., Docker, Vagrant, ...)
2. Provisioning (e.g., Open Stack, Kubernetes, ...)
3. Configuration (e.g., Ansible, Chef, Puppet, ...)
4. Continuous Integration/Continuous Development (e.g., Jenkins, Travis, ...)
5. Monitoring (e.g., ELK, Prometheus+Graphana, ...).

# Our team choises

Our application is based on containerization with Dockers. 
We have created a MySQL server on a Docker, that contains a database with products and their prices a small market sells.
On another docker we have defined a Python app that communicate with the server and interacts with the user via command line. The app requests an input with the name of a product the user wants to buy and another input with the quantity of that product. The app creates a connection to the MySQL database running on another Docker profit by the internal Docker Network, qeuries the db and calculates the recepit to pay.
